import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Map, Marker, Popup, Pane,TileLayer,Polygon, LayerGroup,FeatureGroup,LayersControl, Rectangle, Circle, Tooltip} from 'react-leaflet';
// import Spiderfy from "./Spiderfy";
// import L from "leaflet";
import { businessTableAction } from "../../Actions/userActions";
import Location from "../../Images/location.svg";
import MarkerClusterGroup from  "./react-leaflet-markercluster";
import _ from "lodash";
import L from "leaflet";

const { BaseLayer, Overlay } = LayersControl;

// import Location from "../Images/location.svg";

export const pointerIcon = new L.Icon({
  iconUrl: Location,
    iconSize: [30,45],
})





export class MapLayer extends Component {

  clickedMarker = (e) => {
    console.log("Clicked",e);

  
        if(this.props.loadingBusiness){
          console.log("Yes...");
          return;
        }

      
      let existing = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

      console.log(existing);

      // / Remove existing rows that are highlightied.....
          for(let x = 0; x < existing.children.length; x++){
                  
            if(existing.children[x].classList.contains("show")){
              console.log("YEP EXISTS....");
              existing.children[x].classList.remove("show");
            }
          }



          let element = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[1]/div/div[1]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

          var rows = element.querySelectorAll('tr');


      console.log(rows);

      rows.forEach((row,index) => {
  
        console.log(row.innerHTML,index);
  
  
        if(row.innerHTML.indexOf(e.Name) > -1){
            console.log("yes", row.offsetTop);
            row.classList.add("show");

            // let element6 = document.querySelector(".table-wrapper-scroll-y");
            console.log(element.height);
      
            // element.scrollTo({top: "200px"});
            // element.scrollTo({top: row.offsetTop + 0});

            document.querySelector('.dataTable_scrollBody').scrollTop = row.offsetTop;

            this.props.businessTableAction(e);
  
         
      }
      })








  }

  componentDidMount(){

    let elements = document.querySelectorAll(".leaflet-control-layers-selector");

    console.log(elements);

  }


    render() {


  


        console.log(this.props);
        return (
        <Map  
        
        id="map" 
        // animate={true}
        className="markercluster-map"
            // zoom={6}
        maxZoom={18}
        bounds={this.props.polygon} 
        center={this.props.position}  
        zoomControl={false} 
        >  
      <LayersControl position="topright">

<BaseLayer checked name="Map">

              <TileLayer
            url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          /> 
  </BaseLayer>


<Overlay checked name="Clusters">
<LayerGroup>
<MarkerClusterGroup>
                {this.props.businessTable.map((business,index) => (
                      <Marker onClick={this.clickedMarker.bind(this,business)} position={[business.latitude, business.longitude]}>
                      <Tooltip>
                    
                                    <p style={{textAlign: "center", fontSize: "10px"}}>{business.Name}</p>
                        <p style={{textAlign: "center", fontSize: "10px"}}>{business.Category}</p>
                      
                      </Tooltip>               
                    </Marker>

                    )
                  )}
            </MarkerClusterGroup>

            </LayerGroup>

</Overlay>



<Overlay  name="Chosen">
<LayerGroup>

                  {this.props.coords.length > 0 && (

                <Marker icon={pointerIcon} position={[this.props.coords[0], this.props.coords[1]]}>
                <Tooltip>

                   {this.props.business.length > 0 && (
                     this.props.business.map((business,index) => (
                     <React.Fragment>
                         <p style={{textAlign: "center", fontSize: "10px"}}>{business.Organisation}</p>
                       <p style={{textAlign: "center", fontSize: "10px"}}>{business.Category}</p>
                       </React.Fragment>
                     ))
                   )}
                </Tooltip>               
                </Marker>
)}
   

            </LayerGroup>

</Overlay>


        {!this.props.default && (
        <Polygon color="#7468BA" stroke={true} weight="1"  positions={this.props.polygon} />
        )}
    
    
          </LayersControl>
       </Map>)   
    }
}

const mapStateToProps = (state) => ({
    position: state.user.position,
    polygon: state.user.polygon,
    default: state.user.default,
    coords: state.user.mapCoords,
    business: state.user.business,
    businessTable: state.user.businessTable,
    loadingBusiness: state.user.loadingBusiness
})



export default connect(mapStateToProps, {businessTableAction})(MapLayer)

  


