import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Map, Marker, Popup, Pane,TileLayer,Polygon, LayerGroup,FeatureGroup,LayersControl, Rectangle, Circle, Tooltip} from 'react-leaflet';
import MarkerClusterGroup from  "./react-leaflet-markercluster";



import { grantsTableAction } from "../../Actions/userActions";



const { BaseLayer, Overlay } = LayersControl;

export class MapResearch extends Component {


    clickedMarker = (e) => {
        console.log("Clicked",e);



        let existing = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

        console.log(existing);
  
        // / Remove existing rows that are highlightied.....
            for(let x = 0; x < existing.children.length; x++){
                    
              if(existing.children[x].classList.contains("show")){
                console.log("YEP EXISTS....");
                existing.children[x].classList.remove("show");
              }
            }
  





          let element = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

          var rows = element.querySelectorAll('tr');


      console.log(rows);

      rows.forEach((row,index) => {
  
        console.log(row.innerHTML,index);
  
  
        if(row.innerHTML.indexOf(e.OrganisationName) > -1){
            console.log("yes", row.offsetTop);
            row.classList.add("show");

            // let element6 = document.querySelector(".table-wrapper-scroll-y");
            console.log(element.height);
      
            // element.scrollTo({top: "200px"});
            // element.scrollTo({top: row.offsetTop + 0});

            document.querySelectorAll('.dataTable_scrollBody')[1].scrollTop = row.offsetTop;

            // this.props.businessTableAction(e);

            let topTableData = this.props.grantsData.filter((row,index) => row["Award Recipient"] === e.OrganisationName);

            console.log(topTableData);


            if(topTableData.length > 0){

            let user = {
                LeadROName: topTableData[0]['Award Recipient'],
                FundingOrgName: topTableData[0].Funder
            }
  
  
            this.props.grantsTableAction(user);
      

            }

         
  
         
      }
      })






    }




    render() {
        return (
        <Map 
        id="map" 
        // animate={true}
        className="markercluster-map"
            // zoom={6}
        maxZoom={18}
        bounds={this.props.polygon} 
        center={this.props.position}  
        zoomControl={false} 
        
        >

<LayersControl position="topright">

<BaseLayer checked name="Maps">

              <TileLayer
            url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
            attribution="&copy; <a href=&quot;http://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />    
</BaseLayer>

{/* 
<Overlay checked name="Clusters">
<LayerGroup>
<MarkerClusterGroup>
                {this.props.businessTable.map((business,index) => (
                      <Marker onClick={this.clickedMarker.bind(this,business)} position={[business.latitude, business.longitude]}>
                      <Tooltip>
                    
                                    <p style={{textAlign: "center", fontSize: "10px"}}>{business.Name}</p>
                        <p style={{textAlign: "center", fontSize: "10px"}}>{business.Category}</p>
                      
                      </Tooltip>               
                    </Marker>

                    )
                  )}
            </MarkerClusterGroup>

            </LayerGroup>

</Overlay>
 */}




  <Overlay checked name="Clusters1">
            <LayerGroup>

            <MarkerClusterGroup>

            {this.props.locationGrants.map((business,index) => (
                      <Marker  onClick={this.clickedMarker.bind(this,business)} position={[business.latitude, business.longitude]}>
                      <Tooltip >
                    
                                    <p style={{textAlign: "center", fontSize: "10px"}}>{business.OrganisationName}</p>
                        <p style={{textAlign: "center", fontSize: "10px"}}>{business.postcode}</p>
                      
                      </Tooltip>               
                    </Marker>

                    )
                  )}
            </MarkerClusterGroup>
            </LayerGroup>

</Overlay> 

{!this.props.default && (
        <Polygon color="#7468BA" stroke={true} weight="1"  positions={this.props.polygon} />
        )}
                      
</LayersControl>
                 
       </Map>)   
    }
}

const mapStateToProps = (state) => ({
    position: state.grants.position,
    polygon: state.grants.polygon,
    default: state.grants.default,
    locationGrants: state.grants.locationGrants,
    grantsData: state.grants.grantsData
})



export default connect(mapStateToProps, {grantsTableAction})(MapResearch)

  


