import React, { Component } from 'react'
import { connect } from 'react-redux'

import { loginAction } from "../../Actions/authActions";

import Navbar from "../Layout/Navbar";

export class Login extends Component {

    state ={
        username: "",
        password: "",
    };

    login = (e) => {
        e.preventDefault();
        // If username or password are empty we alert error to user.
        // Else, we will call the login Action user to fetch the api and update redux store.

       let {username,password} = this.state;
        if(username && password){

            let forms = new FormData();
            forms.append("username", username);
            forms.append("password", password);

            console.log(forms);
            this.props.loginAction(forms, this.props);
        } 
    }

    changeHandler = (e) => {
        this.setState({[e.target.name]: e.target.value})
    }

    render() {
        return (
            <React.Fragment>
                {/* <Navbar {...this.props}/> */}
            <div data-test="login" id="login">
                <div id="overlay"></div>

                <div data-test="login-box" id="login-box">
                <h4>Life Sciences UK</h4><hr/>

                {this.props.loginError !== "" && (
                <div class="alert alert-danger alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                    <strong>Failed!</strong> {this.props.loginError}
                </div>
                )}


                <form onSubmit={this.login}>
                   
                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-user"></i></span>
                    </div>
                    <input required className="form-control" type="text" autoComplete="off" 
                    onChange={this.changeHandler} name="username" value={this.state.username} placeholder="Username"></input>
                    </div><br/>

                    <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1"><i class="fas fa-lock"></i></span>
                    </div>
                    <input required className="form-control" type="text" autoComplete="off" 
                    onChange={this.changeHandler} name="password" value={this.state.password} placeholder="Password" /><br/> 
                    </div> <hr/>
                    <button disabled={this.props.loginError ? true : false} id="login-submit-btn" type="submit" >Login</button>

                </form>
                </div>
            </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    loginError: state.auth.loginError
})


export default connect(mapStateToProps, {loginAction})(Login)