import React, { Component } from 'react'
import { connect } from 'react-redux'

import Navbar from "../Layout/Navbar";


export class Landing extends Component {
    render() {
        return (
            <React.Fragment>
                 <Navbar {...this.props} />

                 <div id="landing">
             
             <div id="landing-box">
                 <h2 id="landing-box-title">Welcome to IDM Life Sciences</h2>
             </div>                
         </div>
            </React.Fragment>
           
        )
    }
}

const mapStateToProps = (state) => ({
    
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Landing)
