import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Link} from "react-router-dom";
import idmlogo from "../../Images/Logo original - white copy.png";

export class Navbar extends Component {

    state = {
        loggedIn: false,

    }

    logout = (e) => {
        localStorage.removeItem("ls-access-token");
        localStorage.removeItem("ls-overlay-token");
        window.location.reload();
    }


    logo = () => {
        this.props.history.push("/");
    }


    render() {
        return (
            <div id="navbar">
            <div id="navbar-left">
                <img onClick={this.logo}  src={idmlogo} alt="IDM LOGO" />
            </div>
            <div id="navbar-right">
               {this.props.loggedIn && (
                   <React.Fragment>
                    <Link className="nav-link" to="/dashboard">Dashboard</Link>
                    <Link onClick={this.logout} className="nav-link" to="/">Logout</Link>
                   </React.Fragment>
               )}


                {!this.props.loggedIn && (
                   <React.Fragment>
                 
                    <Link className="nav-link" to="/login">Login</Link>
                   </React.Fragment>
               )}
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => ({
    loggedIn: state.auth.loggedIn
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar)
