import React, { Component } from 'react'
import { connect } from 'react-redux'
import Chart from "react-apexcharts";
import ReactApexChart from "react-apexcharts";


export class BusinessChart extends Component {


    state = {
      
    }


    render() {
        console.log(this.props);
        return (
            <div className="card">
                    <div className="card-header">
                        {this.props.location === "" ? <h6>Business Chart</h6> : <h6>Number of Businesses in {this.props.location}</h6>}
                    </div>
                   
                    <Chart
                                        id="chart1"
                                        options={this.props.businessOptions}
                                        series={this.props.series}
                                        type="bar"
                                        height = "87%"
                                        
                                  />
                   
                  
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    series: state.user.businessChart,
    businessOptions: state.user.businessOptions,
    location: state.user.location
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(BusinessChart)
