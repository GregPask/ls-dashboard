import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from "lodash";
import { MDBDataTable } from 'mdbreact';

import {grantsTableAction} from "../../Actions/userActions";

import ReactTable from "react-table";
import "react-table/react-table.css";
export class GrantTable1 extends Component {

    state = {
        
    }
    changePage = (e) => {
      console.log("chaiong page...")

      let table = document.evaluate(`//*[@id="cardyb"]/div[2]/div/div/div[1]/div[3]`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      console.log(table);
      table.scrollTop = 0;


    } 


    scrollTop = (e) => {
      console.log("Scrolling top...");


      let table = document.evaluate(`//*[@id="cardyb"]/div[2]/div/div/div[1]/div[3]`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      console.log(table);
      table.scrollTop = 0;

    }
    
    clickTable = (e) => {

        console.log("Clicked grant table", e);


      // Remove both grants 1 and grants 2 table.....


      let existing = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

      // Remove existing rows that are highlightied.....
        for(let x = 0; x < existing.children.length; x++){
                
          if(existing.children[x].classList.contains("show")){
            console.log("YEP EXISTS....");
            existing.children[x].classList.remove("show");
          }
        }





        let existing2 = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

        // Remove existing rows that are highlightied.....
          for(let x = 0; x < existing2.children.length; x++){
                  
            if(existing2.children[x].classList.contains("show")){
              console.log("YEP EXISTS....");
              existing2.children[x].classList.remove("show");
            }
          }



      //  ---------------------------------------------------------


  let element = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;



        var rows = element.querySelectorAll('tr');


        rows.forEach(row => {

          console.log(row.innerHTML);


          if(row.innerHTML.indexOf(e[0]['Award Recipient']) > -1){
            console.log("yes", row);


              row.classList.add("show");


              let user = {
                LeadROName: e[0]['Award Recipient'],
                FundingOrgName: e[0].Funder
            }
  
  
            this.props.grantsTableAction(user);
      
        }
        })


       
      // let existing = document.evaluate(`//*[@id="cardyb"]/div[2]/div/div/div[1]/div[3]`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
      // console.log(existing)
      //   // / Remove existing rows that are highlightied.....
      //     for(let x = 0; x < existing.children.length; x++){
                  
      //       if(existing.children[x].classList.contains("show")){
      //         console.log("YEP EXISTS....");
      //         existing.children[x].classList.remove("show");
      //       }
      //     }


      // var rows = existing.querySelectorAll('.rt-tr-group');


      // console.log(rows);

      // rows.forEach((row,index) => {
  
      //   // console.log(row.innerHTML,index);
  
  
      //   if(row.innerHTML.indexOf(e.original.LeadROName) > -1){
      //     console.log("yes", row.offsetTop);
  
      //       row.classList.add("show");
      //       // existing.scrollTo({top: row.offsetTop - 100});

      // }
      // })

    }





    render() {
      
      console.log(this.props.grantsData);
      

      function numberWithCommas(x) {
        return "£" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }
         

        let ro2 = this.props.grantsData.map((row,index) => {
            console.log(row);
            return {
              FundingOrgName: row.Funder,
              LeadROName: row['Award Recipient'],
            
              AwardPounds: numberWithCommas(row.Value),
              clickEvent: this.clickTable.bind(this,[row,index]),
           
            }

        })
        console.log(ro2);


        const data = {
            columns: [
            {
              label: 'Funder',
              field: 'FundingOrgName',
            //   width: 40 
            },
            {
              label: 'Recipient',
              field: 'LeadROName',
              sort: 'asc',
            //   width: 5000
            },
            {
              label: 'Value',
              field: 'AwardPounds',
              sort: 'asc',
            //   width: 40
            }
            ],
            rows: ro2
            };



  


          const columns2 = [{
            Header: 'Funder',
            accessor: 'FundingOrgName' // String-based value accessors!
          }, 
          {
            Header: 'Recipient',
            accessor: 'LeadROName',
          }, 
          {
            Header: 'Value',
            accessor: 'AwardPounds',
          }
        ]

          console.log(ro2.length);

        return (
            <div style={{overflow: "hidden"}}  id="cardyb" className="card">
            {/* <div className="card-header">
              <h6>Grants Held in Area</h6>
            </div> */}
              <div id="show-card"  style={{overflow: "hidden"}} className="h-100 card-body">
              {this.props.location !== "" && (  <h6 style={{position: "absolute"}} className="">Grants held in {this.props.location} ({this.props.loadingGrants ? "0" : this.props.grantsData.length} results) </h6>)} 


              {this.props.location === "" && (  <h6 style={{position: "absolute"}} className="">Grants held in Location </h6>)} 
            
              <div className="card-wrapper">
              {/* {this.props.location !== "" && (  <h6 style={{position: "absolute", top: "0px", margin: "14px 0 0 10px"}} className="text-muted">Grants Held by {this.props.location}</h6>)} */}
                <MDBDataTable
                       scrollX
                        striped
                        fixed
                        paging={false}
                        small
                        
                        data={data}
             
                />
            </div>

              
{/* 
                <ReactTable
                className="-striped -highlight"
              data={ro2}
              columns={columns2}
              filterable={true}
              showPagination={false}
              pageSize = {ro2.length}
              multiSort={false}
              loading={this.props.loadingGrants}
              onPageChange = {this.changePage}
              onFilteredChange = {this.scrollTop}
              getTdProps={(state, rowInfo, column, instance) => {
      return {
        onClick: (e, handleOriginal) => {
            console.log("Called...");
            this.clickedTable(rowInfo);
        }
      }
    }}      
   
    
  /> */}
             
              </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    grantsData: state.grants.grantsData,
    location: state.grants.location,
    loadingGrants: state.grants.loadingGrants
})



export default connect(mapStateToProps, {grantsTableAction})(GrantTable1)
