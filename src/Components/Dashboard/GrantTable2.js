import React, { Component } from 'react'
import { connect } from 'react-redux'
import _ from "lodash";
import { MDBTable,MDBDataTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { Spinner } from 'reactstrap';


import ReactTable from "react-table";
import "react-table/react-table.css";

import { grantsTable2Action} from "../../Actions/userActions";
export class GrantTable2 extends Component {

    state = {
        
    }

    clickTable = (e) => {

      // We will pass the project reference number to get related grants and publications....

        console.log("Clicked 2nd table", e);
            let users = {
              id: e[0]["Reference Number"]
            }



            let existing = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

            // Remove existing rows that are highlightied.....
              for(let x = 0; x < existing.children.length; x++){
                      
                if(existing.children[x].classList.contains("show")){
                  console.log("YEP EXISTS....");
                  existing.children[x].classList.remove("show");
                }
              }

              console.log(e[0]["Award Recipient"]);

              let element = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;
              let rows = element.querySelectorAll('tr');
      
              console.log(rows);
              rows.forEach(row => {
      
                console.log(row.innerHTML);
      
      
                if(row.innerHTML.indexOf(e[0]['Lead Investigator']) > -1){
                  console.log("yes", row);
      
      
                    row.classList.add("show");      
        
                 
            
              }
              })

              this.props.grantsTable2Action(users);
      

    }
    render() {
    
      //  console.log(this.props.grantsOrgData);




       function numberWithCommas(x) {
        return "£" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
      }


       let ro = this.props.grantsOrgData.map((row,index) => {
        return {
          FundingOrgName: row.Funder,
          ProjectReference: row["Reference Number"] === null ? "-- not provided --" : row["Reference Number"],
          LeadROName: row["Lead Investigator"],
          Department: row.Department === null ? "not provided" : row.Department,
          Value: numberWithCommas(row.Value),
          clickEvent: this.clickTable.bind(this,[row,index]),
        }
      });
      
      const data = {
      columns: [
        {
        label: 'Principal Investigator',
        field: 'LeadROName',
        sort: 'asc',
        width: 80
      },
      {
        label: 'Department',
        field: 'Department',
        sort: 'asc',
        width: 70
      },
      {
        label: 'Value',
        field: 'Value',
        sort: 'asc',
        width: 40
      }
      ],
      rows: ro
      };


      const columns2 = [{
        Header: 'Principal Investigator',
        accessor: 'LeadROName' // String-based value accessors!
      }, 
      {
        Header: 'Department',
        accessor: 'Department',
      }, 
      {
        Header: 'Value',
        accessor: 'Value',
      }
    ]



        return (
            this.props.grantsOrgData === [] ? (<div>Nothing</div>) : (  
            <div style={{overflow: "hidden"}} className="card">
            
             {this.props.grantsOrgData.length === 0 && (  <h6 style={{position: "absolute", top: "10px", left: "20px"}}></h6>)} 

             {this.props.grantsOrgData.length > 0 && (  <h6 style={{position: "absolute", top: "10px", left: "20px"}}>Funding Orgs ({this.props.grantsOrgData.length} results)</h6>)} 

                <div className="card-body">
                    <div className="card-wrapper">
                        {this.props.loading && (      <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div>
                            <div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>)}
                            <MDBDataTable
                              scrollX
                                striped
                                fixed
                                paging={false}
                                small
                                
                                data={data}
                    
                        />
                    </div>

                </div>
        </div>)
        )
    }
}

const mapStateToProps = (state) => ({
    grantsData: state.grants.grantsData,
    grantsOrgData: state.grants.grantsOrgData,
    loading: state.grants.loadingOrgGrants
})



export default connect(mapStateToProps, {grantsTable2Action})(GrantTable2)
