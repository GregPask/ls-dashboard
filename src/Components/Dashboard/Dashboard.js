import React, { Component } from 'react'
import { connect } from 'react-redux'


import Navbar from "../Layout/Navbar";
import DashboardRight from "./DashboardRight";
import DashboardLeft from "./DashboardLeft";
import DashboardLeftResearch from "./DashboardLeftResearch";


import screen1 from "../../Images/screen1.png";
import screen2 from "../../Images/screen2.png";

import { closeOverlayAction, overlayAction} from "../../Actions/InterfaceActions";

class Dashboard extends Component {


    state = {

    }


    dismissOverlay = () => {
        // alert("Closing...");
        this.props.closeOverlayAction();
        
    }

    componentDidMount(){

        setTimeout(() => {
            this.props.overlayAction();
        },800);

    }

    render() {

        

        return (
            <React.Fragment>
                 <Navbar {...this.props} />
            <div id="dashboard">

                <div className={this.props.overlay === true ? "show" : ""} id="overlay">
                    {this.props.overlay && (
                        <div id="overlay-box">
                                    <button onClick={this.dismissOverlay} id="overlay-skip-btn">Skip</button>
                                   <h1>Welcome to IDM Life Sciences</h1>

                                <p>some random content to display here............................</p>

                                  
                                   <button id="submit-overlay" onClick={this.dismissOverlay} className="btn">Close</button>
                        </div>
                    )}
                </div> 


                        {this.props.page === "Business" && (<DashboardLeft page={this.props.page} />)}

                        {this.props.page === "Research" && (<DashboardLeftResearch page={this.props.page}/>)}
                <DashboardRight />
            </div>
            </React.Fragment>
        )
    }
}

const mapStateToProps = (state) => ({
    overlay: state.interface.overlay,
    page: state.interface.tab
})



export default connect(mapStateToProps, {overlayAction, closeOverlayAction})(Dashboard)
