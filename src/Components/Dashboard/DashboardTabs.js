import React, { Component } from 'react'

const DashboardTabs = ({changePage}) => {
    console.log(changePage);
    return (
        // <div id="dashboard-right-nav">   
         
            <ul id="dashboard-right-ul" className="nav nav-pills" role="tablist">
                <li className="nav-item">
                    <a onClick={() => changePage("Business")} className="nav-link active" data-toggle="pill" href="#tab-1">Business</a>
                </li>
                <li className="nav-item">
                    <a onClick={() => changePage("Research")} className="nav-link" data-toggle="pill" href="#tab-2">Research</a>
                </li>
            </ul>
        // </div>
    )
}


export default DashboardTabs;