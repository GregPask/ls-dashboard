import React, { Component } from 'react'
import { connect } from 'react-redux'
import DashboardTabs from "./DashboardTabs";
import axios from "axios";


import IDMLOGO from "../../Images/Logo original - white copy.png";
import BusinessChart from "./BusinessChart";
import BusinessTable from "./BusinessTable";

import GrantsChart from "./GrantsChart";
import GrantTable1 from "./GrantTable1";

import GrantTable2 from "./GrantTable2";
import RelatedGrants from "./RelatedGrants";
import { changePageAction } from "../../Actions/InterfaceActions";
import _ from "lodash";


class DashboardLeft extends Component {


    state = {

    }


    render() {
        console.log(this.props.business);


        function numberWithCommas(x) {
            return "£" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
          }
    



        let {business} = this.props;

        return (
                <div id="dashboard-right">
 


                    <div className="tab-content">
                  
                  
                        <div id="tab-1" className="tab-pane active" role="tabpanel">



                   
                            <div className="dashboard-right-grid1">
                            <BusinessTable  />
                                <BusinessChart  />


                                 
                                <div id="dashboard-right-info" className="card">   
                                    <div className="card-header">
                                        {/* <h6>Company Info</h6> */}
                                        {this.props.business.length > 0 && (<h6>{this.props.business[0].Organisation}, {this.props.business[0]["Company Number"]}.</h6>)}
                                        {this.props.business.length === 0 && (<h6>Company Info</h6>)}
                                    </div>
                                    
                                    {this.props.loadingBusiness && (  
                                    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                                    
                                    )}


                                    {(!this.props.loadingBusiness && Object.keys(this.props.business).length  === 0 ) && (
                                            <div style={{height: "100%",display: "flex", flexDirection: "column", justifyContent: "center", alignItems: "center"}}>
                                                <h1 className="text-muted">Click Company to View</h1>
                                                {/* <img style={{width: "100%"}} src={IDMLOGO} /> */}
                                            </div>
                                    )}

                                       {!this.props.loadingBusiness && (
                                            Object.keys(this.props.business).length > 0  && (
                                                <div id="info-section" style={{overflow: "hidden !important"}} className="card-body" >
                                                <div style={{overflow: "scroll"}} className="card-wrapper">


                                                <label>Description</label>
                                                            <p>{_.has(business,"[0].Description") ? business[0].Description === null ? "-- not provided --" : business[0].Description : "-- not provided --"}</p>
                                                        <br/>
                                                    <div className="row">

                                                    
                                                        <div className="col-lg-6">
                                                     
                                                            {/* <label>Organisation</label>
                                                            <p>{_.has(business,"[0].Organisation") ? business[0].Organisation === null ? "null" : business[0].Organisation  : "-- not provided --"}</p> */}





                                                            <label>Operating Postcode</label>
                                                            <p>{_.has(business,"[0]['Operating Postcode']") ? business[0]['Operating Postcode'] === null ? "-- not provided --" : business[0]['Operating Postcode'] : "-- not provided --"}</p>



                                                            <label>Registered Address Postcode</label>
                                                            <p>{_.has(business,"[0]['Registered Address Postcode']") ? business[0]['Registered Address Postcode'] === null ? "-- not provided --" :  business[0]['Registered Address Postcode'] : "-- not provided --"}</p>



                                                            <label>Address Line 1</label>
                                                            <p>{_.has(business,"[0][' Address Line 1']") ? business[0]['Address Line 1'] === null ? "-- not provided --" :  business[0]['Address Line 1'] : "-- not provided --"}</p>



                                                            <label> Address Line 2</label>
                                                            <p>{_.has(business,"[0][' Address Line 1']") ? business[0]['Address Line 1'] === null ? "-- not provided --" :  business[0]['Address Line 1'] : "-- not provided --"}</p>




                                                            <label>Category</label>
                                                            <p>{_.has(business,"[0]['Category']") ? business[0]['Category'] === null ? "-- not provided --" :  business[0]['Category'] : "-- not provided --"}</p>


                                                            <label>Company Type</label>
                                                            <p>{_.has(business,"[0]['Company Type']") ? business[0]['Company Type'] === null ? "-- not provided --" :  business[0]['Company Type'] : "-- not provided --"}</p>



                                                            <label>Status</label>
                                                            <p>{_.has(business,"[0]['Status']") ? business[0]['Status'] === null ? "-- not provided --" :  business[0]['Status'] : "-- not provided --"}</p>



                                                            <label> Date Incorporated</label>
                                                            <p>{_.has(business,"[0]['Date Incorporated']") ? business[0]['Date Incorporated'] === null ? "-- not provided --" :  business[0]['Date Incorporated'] : "-- not provided --"}</p>


                                                            <label> Home Country</label>
                                                            <p>{_.has(business,"[0][' Home Country']") ? business[0]['Home Country'] === null ? "-- not provided --" : business[0]['Home Country'] : "-- not provided --"}</p>

                                                            <label>SIC Code</label>
                                                            <p>{_.has(business,"[0]['SIC Code']") ? business[0]['SIC Code'] === null ? "-- not provided --" : business[0]['SIC Code'] : "-- not provided --"}</p>


                                                            <label>Next Accounts Due</label>
                                                            <p>{_.has(business,"[0][' Next Accounts Due']") ? business[0][' Next Accounts Due'] === null ? "-- not provided --" : business[0][' Next Accounts Due'] : "-- not provided --"}</p>


                                                            <label>Last Accounts</label>
                                                            <p>{_.has(business,"[0]['Last Accounts']") ? business[0]['Last Accounts'] === null ? "-- not provided --" : business[0]['Last Accounts'] : "-- not provided --"}</p>

                                                            <label> Accounts Type</label>
                                                            <p>{_.has(business,"[0]['Accounts Type']") ? business[0]['Accounts Type'] === null ? "-- not provided --" :  business[0]['Accounts Type'] : "-- not provided --"}</p>
                                                        </div>

                                                        <div className="col-lg-6">
                                                     
                                                        <label>Company Number</label>
                                                            <p>{_.has(business,"[0]['Company Number']") ? business[0]['Company Number'] === null ? "-- not provided --" : business[0]['Company Number'] : "-- not provided --"}</p>


                                                        <label>Equity Value</label>
                                                            <p>{_.has(business,"[0]['Equity Value']") ? business[0]['Equity Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Equity Value']) : "-- not provided --"}</p>



                                                            {/* <label> Debtors Value Unit </label>
                                                            <p>{_.has(business,"[0][' Debtors Value Unit']") ? business[0][' Debtors Value Unit'] === null ? "-- not provided --" : business[0][' Debtors Value Unit'] : "-- not provided --"}</p> */}


                                                            <label>Debtors Value </label>
                                                            <p>{_.has(business,"[0]['Debtors Value']") ? business[0]['Debtors Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Debtors Value']) : "-- not provided --"}</p>


                                                            <label>Other Debtors Value </label>
                                                            <p>{_.has(business,"[0]['Other Debtors Value']") ? business[0]['Other Debtors Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Other Debtors Value']) : "-- not provided --"}</p>


                                                            {/* <label>Other Debtors Value Unit </label>
                                                            <p>{_.has(business,"[0]['Other Debtors Value Unit']") ? business[0]['Other Debtors Value Unit'] === null ? "-- not provided --" : business[0]['Other Debtors Value Unit'] : "-- not provided --"}</p> */}



                                                            <label>Creditors Value </label>
                                                            <p>{_.has(business,"[0]['Creditors Value']") ? business[0]['Creditors Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Creditors Value']) : "-- not provided --"}</p>


                                                    
                                                            {/* <label>Creditors Value Unit </label>
                                                            <p>{_.has(business,"[0]['Creditors Value Unit']") ? business[0]['Creditors Value Unit'] === null ? "-- not provided --" : business[0]['Creditors Value Unit'] : "-- not provided --"}</p> */}



                                                            <label>Current Assets Value </label>
                                                            <p>{_.has(business,"[0]['Current Assets Value']") ? business[0]['Current Assets Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Current Assets Value']) : "-- not provided --"}</p>



                                                            <label>Current Assets Value Unit </label>
                                                            <p>{_.has(business,"[0]['Current Assets Value Unit']") ? business[0]['Current Assets Value Unit'] === null ? "-- not provided --" : business[0]['Current Assets Value Unit'] : "-- not provided --"}</p>




                                                            <label>Cash at Bank and on Hand Assets Value </label>
                                                            <p>{_.has(business,"[0]['Cash at Bank and on Hand Assets Value']") ? business[0]['Cash at Bank and on Hand Assets Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Cash at Bank and on Hand Assets Value']) : "-- not provided --"}</p>


{/* 
                                                            <label>Cash at Bank and on Hand Assets Value Unit </label>
                                                            <p>{_.has(business,"[0]['Cash at Bank and on Hand Assets Value Unit']") ? business[0]['Cash at Bank and on Hand Assets Value Unit'] === null ? "-- not provided --" : business[0]['Cash at Bank and on Hand Assets Value Unit'] : "-- not provided --"}</p> */}



                                                            <label>Deferred Income Value </label>
                                                            <p>{_.has(business,"[0]['Deferred Income Value']") ? business[0]['Deferred Income Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Deferred Income Value']) : "-- not provided --"}</p>



                                                            {/* <label>Deferred Income Value Unit </label>
                                                            <p>{_.has(business,"[0]['Deferred Income Value Unit']") ? business[0]['Deferred Income Value Unit']  === null ? "-- not provided --" : business[0]['Deferred Income Value Unit'] : "-- not provided --"}</p> */}



                                                            <label>Net Assets/Liabilities Value </label>
                                                            <p>{_.has(business,"[0]['Net Assets/Liabilities Value']") ? business[0]['Net Assets/Liabilities Value'] === null ? "-- not provided --" : numberWithCommas(business[0]['Net Assets/Liabilities Value']) : "-- not provided --"}</p>


{/* 
                                                            <label> Net Assets/Liabilities Value Unit </label>
                                                            <p>{_.has(business,"[0][' Net Assets/Liabilities Value Unit']") ? business[0][' Net Assets/Liabilities Value Unit'] === null ? "-- not provided --" :  business[0][' Net Assets/Liabilities Value Unit'] : "-- not provided --"}</p> */}
                                                        </div>
                                                    </div>
                                                                                           
                                                </div>
                                                </div>
                                            )
                                       )}
                                 
                                    </div>                                                           
                            </div>                          
                        </div>


                        <div id="tab-2" className="tab-pane fade">          
                            <div className="dashboard-right-grid2">
                                <GrantsChart />        
                                <GrantTable1/>
                                <GrantTable2/>                                                   
                                <RelatedGrants />                          
                            </div>
                        </div>
                    </div>
                </div>
        )
    }
}

const mapStateToProps = (state) => ({
    business: state.user.business,
    loadingBusiness: state.user.loadingBusiness
})



export default connect(mapStateToProps, {changePageAction})(DashboardLeft)
