import React, { Component } from 'react'
import { connect } from 'react-redux';
import axios from "axios";


import { searchAction, grantsSearchAction } from "../../Actions/userActions";

import MapLayer from "../Map/Map";
import MapResearch from "../Map/MapResearch";
import DashboardTabs from "./DashboardTabs";


import { changePageAction } from "../../Actions/InterfaceActions";

export class DashboardLeft extends Component {

    state = {
        Countries: ["England","Scotland","Wales","Northern Ireland"],
        areaType: [],
        region: [],
        code: "",
        displayRegion: "East Midlands",
    }


      componentWillMount(){

        console.log("TABS WILL MOUNT....");

        let token = localStorage.getItem("ls-access-token");

        axios.get('https://insights.impactdatametrics.com:17475/api/custom/areasC/',{
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then((res) => {
            // console.log(res.data);
            let england = res.data.filter((array5) => array5[4] === "England");
            let country = "England";
            let areaType = [];


        //Poplate the area types of the given country....
        for(let x in england){
            if(!areaType.includes(england[x][3])){
                areaType.push(england[x][3]);
            }
        }

        //Sort region by Alphabetical order...
        let region = england.filter((array5) => array5[3] === "English Region");
        region.sort((a,b) => b[2] > a[2] ? -1 : 1);
        
        // console.log(region);

        this.setState({
            Selectedcountry: country,
            areaType,
            region,
            allData: res.data,
            countryData: england,
            code: region[0][1]
        });           
        })
        .catch((err) => console.log(err));
    }





    changeCountry = (e) => {

        let allData = this.state.allData.slice();
        let country = allData.filter((array5,index) => array5[4] === e.target.value);
        let areaType =[];

        for(let x in country){
        if(!areaType.includes(country[x][3])){
            areaType.push(country[x][3]);
        } 
        }

        let region = country.filter((array5,index) => array5[3] === areaType[0]);      
        region.sort((a,b) => b[2] > a[2] ? -1 : 1);        

        let code = region[0][1];
        let displayRegion =  region[0][2];


        console.log(region, displayRegion);


        this.setState({
        countryData: country,
        areaType,
        region,
        selectCounty: e.target.value,
        displayRegion,
        code,
        })
    }


    changeArea = (e) => {

        let countryData = this.state.countryData.slice();
        let matches = countryData.filter((array5,index) => array5[3] === e.target.value);  
        matches.sort((a,b) => b[2] > a[2] ? -1 : 1);
        
        let code = matches[0][1];
        let displayRegion = matches[0][2];

        this.setState({
            region: matches, 
            code, 
            displayRegion
        })
    }


    regionChange = (e) => {
        let countryData = this.state.countryData.slice();
        let matches = countryData.filter((array5,index) => array5[1] === e.target.value);
        let code = e.target.value;
       
        this.setState({
            code: e.target.value, 
            displayRegion: matches[2],
        })
    }



    // Clicked Search Button -----------

    companySearch = (e) => {
        let config = {
            code: this.state.code
        }
        this.props.searchAction(config);
    }

    grantsSearch = (e) => {
        this.props.grantsSearchAction(this.state.code);


        // Remove both grants 1 and 2 table.....

        let existing = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[2]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

        // Remove existing rows that are highlightied.....
          for(let x = 0; x < existing.children.length; x++){
                  
            if(existing.children[x].classList.contains("show")){
              console.log("YEP EXISTS....");
              existing.children[x].classList.remove("show");
            }
          }



        
          let existing2 = document.evaluate(`/html/body/div/div/div[2]/div[3]/div/div[2]/div/div[3]/div/div/div/div[2]/div/div/div[2]/div/table/tbody`, document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue;

          // Remove existing rows that are highlightied.....
            for(let x = 0; x < existing2.children.length; x++){
                    
              if(existing2.children[x].classList.contains("show")){
                console.log("YEP EXISTS....");
                existing2.children[x].classList.remove("show");
              }
            }
    }

    render() {
        // console.log(this.props.business);
        console.log("Loading Business", this.props.loadingBusiness);
        return (
            <div className="card" id="dashboard-left">

            <div className="card-header">
            
                                 

              <ul id="dashboard-right-ul" className="nav nav-pills" role="tablist">
                        <li className="nav-item">
                            <a onClick={() => this.props.changePageAction("Business")} className="nav-link" data-toggle="pill" href="#tab-1">Business</a>
                        </li>
                        <li className="nav-item">
                            <a onClick={() => this.props.changePageAction("Research")} className="nav-link active" data-toggle="pill" href="#tab-2">Research</a>
                        </li>
                    </ul>

            </div>
              <div className="card-body">

                  


                      
                           <div className="dashboard-left-flex">
                           <label className="text-muted">Choose Country</label>
                           <div className="input-border">
                           <select data-test="select-country"  ref={e => this.countryRef = e} className="form-control" onChange={this.changeCountry}>
                               {this.state.Countries.map((country,index) => (
                               <option key={index} value={country}>{country}</option>
                               ))}           
                           </select>
                           </div>
                       </div><br/>
       
       
       
                       <div className="dashboard-left-flex">
                           <label className="text-muted">Choose Area</label>
                           <div className="input-border">
                       
                           <select data-test="select-area" ref={e => this.areaRef = e}  className="form-control" onChange={this.changeArea}>
                               {this.state.areaType.map((area,index) => (
                               <option  key={index} value={area}>{area}</option>
                               ))}
                           </select>
                           </div>
                       </div><br/>
       
       
       
       
       
                      <div className="dashboard-left-flex">
                           <label className="text-muted">Choose Region</label>
                           <div className="input-border">
                       
                           <select ref={e => this.regions = e} data-test="select-region" value={this.state.displayRegion} className="form-control"  onChange={this.regionChange}> 
                           {this.state.region.map((area,index) => (
                           <option name={area[1]} key={index} value={area[1]}>{area[2]}</option>
                           ))}
                           </select>
                           </div>
                       </div><br/>
                 
                    
                   {/* {this.props.tab === "Business" && ( <button style={{backgroundColor: this.props.loadingBusiness ? "#ccc" : ""}} disabled={this.props.loadingBusiness} onClick={this.companySearch} id="dashboard-left-search">Search</button>)} */}
                   {this.props.tab === "Research" && ( <button disabled={this.props.disableBtn} onClick={this.grantsSearch} id="dashboard-left-search">Grants Search</button>)}


                  

                   {this.props.tab === "Research" && (<MapResearch />)}


               </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    business: state.user.business,
    tab: state.interface.tab,
    loadingBusiness: state.user.loadingBusiness,
    disableBtn: state.grants.loadingGrants
})



export default connect(mapStateToProps, {searchAction, changePageAction, grantsSearchAction})(DashboardLeft);
