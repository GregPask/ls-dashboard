import React, { Component } from 'react'
import { connect } from 'react-redux'
import Chart from "react-apexcharts";
import ReactApexChart from "react-apexcharts";
import _ from "lodash";
import { Spinner } from 'reactstrap';

export class GrantsChart extends Component {

    state = {

    }

    render() {
        return (
            <div className="card">
            <div className="card-header">
                <h6>Active Grant Awards</h6>
            </div>
                <div className="card-body">
                {this.props.loading && (  
                    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div>
                    <div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                
                )}
           
                <Chart
                                        id="chart1"
                                        options={this.props.grantOptions}
                                        series={this.props.series}
                                        type="bar"
                                        striped
                                        height="100%"
                                      />                
                </div>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    grantOptions: state.grants.grantOptions,
    grantData: state.grants.grantsData,
    loading: state.grants.loadingGrants,
    series: state.grants.series
})

const mapDispatchToProps = {
    
}

export default connect(mapStateToProps, mapDispatchToProps)(GrantsChart)
