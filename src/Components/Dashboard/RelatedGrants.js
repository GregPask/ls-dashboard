import React, { Component } from 'react'
import { connect } from 'react-redux'
import { MDBTable,MDBDataTable, MDBTableBody, MDBTableHead } from 'mdbreact';
import { Spinner } from "reactstrap";



import ReactTable from "react-table";
import "react-table/react-table.css";

export class RelatedGrants extends Component {
    render() {

console.log(this.props.relatedPubs);

function numberWithCommas(x) {
    return "£" + x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  }
  

  // Related Grants --------------------------------------------------

    let ro = this.props.relatedGrants.map((row,index) => {
        return {
        // FundingOrgName: row.FundingOrgName,
        Title: row.Title,
        Recipient: row["Award Recipient"]  === null ? "none provided" : row["Award Recipient"],
        LeadRo: row["Lead Investigator"]  === null ? "none provided" : row["Lead Investigator"],
        Department:  row.Department === null ? "none provided" : row.Department,
        Value:  row.value === null ?  "_" : numberWithCommas(row.Value),
        view: <a className="a" color="blue" size="sm">View</a>,
        }
    });
        
        const data = {
        columns: [
        {
            label: 'Title',
            field: 'Title',
            width: 70 
        },
        {
            label: 'Principal Investigator',
            field: 'LeadRo',
            sort: 'asc',
            width: 80
        },
        {
            label: 'Department',
            field: 'Department',
            sort: 'asc',
            width: 70
        },
        {
            label: 'Value',
            field: 'Value',
            sort: 'asc',
            width: 40
        }
        ],
        rows: ro
        };


        const columns2 = [{
            Header: 'Title',
            accessor: 'Title'
          }, 
          {
            Header: 'Principal Investigator',
            accessor: 'LeadRo',
          }, 
          {
            Header: 'Department',
            accessor: 'Department',
          },
          {
            Header: 'Value',
            accessor: 'Value',
          }
        ]




        // Related Publications ----------------

        let ro2 = this.props.relatedPubs.map((row,index) => {
            return {
            ArticleType: row["Article Type"],
            LeadAuthor: row["Lead Author"] === null ? "none provided" : row["Lead Author"] ,
            Title: row.Title === null ? "none provided" : row.Title ,
            Name:  row["Publication Name"] === null ? "none provided" : row["Publication Name"],
            view: <a qclassName="a" color="blue" size="sm">View</a>,
            }
        });


        const data2 = {
            columns: [
            {
                label: 'ArticleType',
                field: 'ArticleType',
                width: 70 
            },
            {
                label: 'Author',
                field: 'LeadAuthor',
                sort: 'asc',
                width: 50
            },
            {
                label: 'Title',
                field: 'Title',
                sort: 'asc',
                width: 70
            },
            {
                label: 'Name',
                field: 'Name',
                sort: 'asc',
                width: 70
            } 
            ],
            rows: ro2
        }



        const columns3 = [{
            Header: 'ArticleType',
            accessor: 'ArticleType'
          }, 
          {
            Header: 'Author',
            accessor: 'LeadAuthor',
          }, 
          {
            Header: 'Title',
            accessor: 'Title',
          },
          {
            Header: 'Name',
            accessor: 'Name',
          }
        ]



        
        return (
            <div style={{position: "relative"}} id="greg" className="card">
                {/* <div style={{padding: "0rem !important"}} className="card-header"> */}
                <ul style={{position: "absolute", top: "10px", width: "50%", zIndex: "100"}} class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" href="#profile" role="tab" data-toggle="tab">Related Grants</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#buzz" role="tab" data-toggle="tab">Related Publications</a>
                    </li>
                </ul>
                {/* </div> */}<br/>

            <div class="tab-content h-100">
                <div role="tabpanel" class="tab-pane active h-100" id="profile">
                
                    
                        <div className="card-wrapper">


                        {this.props.loadingRelatedGrants && (    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div>
                    <div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>)}
                    <MDBDataTable
                       scrollX
                        striped
                        fixed
                        paging={false}
                        small
                        
                        data={data}
             
                />

                        </div>                       

                
                </div>
                <div  role="tabpanel" class="tab-pane fade" id="buzz">

                            <div className="card-wrapper">
                        {this.props.loadingRelatedGrants && (    <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div>
                <div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>)}
                <MDBDataTable
                       scrollX
                        striped
                        fixed
                        paging={false}
                        small
                        
                        data={data2}
             
                />
                 </div>

                </div>
            </div>
        </div>
        )
    }
}

const mapStateToProps = (state) => ({
    loadingRelatedGrants: state.grants.loadingRelatedGrants,
    relatedGrants: state.grants.relatedGrants,
    relatedPubs: state.grants.relatedPubs
})


export default connect(mapStateToProps, {})(RelatedGrants)
