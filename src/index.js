import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';



import { connect } from "react-redux";
import { combineReducers } from "redux";

import { Provider } from "react-redux";
import { createStore, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";
import reducer from "./Reducers/rootReducer";

import { BrowserRouter} from "react-router-dom";

//Create Store 

const initialState = {};

const store = createStore(reducer, initialState, compose(
    applyMiddleware(thunk)
    // window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
))


ReactDOM.render(
<Provider store={store}>
<BrowserRouter>
<App />
</BrowserRouter>


</Provider>, document.getElementById('root'));
