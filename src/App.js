import React, { Component } from 'react'
import { Link, Switch, Route, Redirect } from "react-router-dom";
import { connect} from "react-redux";

import "./Css/normal.css";
import "./Css/app.scss";
import "./Css/landing.scss";
import "./Css/navbar.scss";
import "./Css/login.scss";
import "./Css/dashboard.scss";
import "./Css/map.scss";
import "./Css/overlay.scss";

// React table config stuff ----

import "@fortawesome/fontawesome-free/css/all.min.css";
import "bootstrap-css-only/css/bootstrap.min.css";
import "mdbreact/dist/css/mdb.css";

import $ from "jquery";


// Components 

import Landing from "./Components/Landing/Landing";
import Navbar from "./Components/Layout/Navbar";
import Login from "./Components/Auth/Login";
import Dashboard from "./Components/Dashboard/Dashboard";
import withAuth from "./withAuth";

//Actions

import { AppStatusAction} from "./Actions/authActions";

import axios from "axios";
class App extends Component {


  state = {
    accessToken: false,
    overlayToken: false,
    refreshToken: false
  }




  componentDidMount(){


 


  

    let { accessToken, overlayToken, refreshToken} = this.state;

    accessToken = localStorage.getItem("ls-access-token");
    overlayToken = localStorage.getItem("ls-overlay-token");
    refreshToken = localStorage.getItem("ls-refresh-token");

    let config = {
      accessToken,
      overlayToken,
      refreshToken
    }

    this.props.AppStatusAction(config);

        // udemy.com
        // UPA-ymt-f5G-dqo
        // geoff.wrainwighticloud.com

  }


  render() {

    console.log(this.props);

    // if(this.props.auth.loggedIn){
    //    window.location.href="/dashboard";
    //    return;
    // }

    
    return (
      <div id="app">
    
          <Switch>
            <Route exact path ="/" component={(Landing)} />
            <Route exact path ="/login" component={(Login)} />
            <Route exact path ="/dashboard" component={withAuth(Dashboard)} />
        
      
            {/* <Route exact path ="/login" component={Login} /> */}

          </Switch>



      </div>
    )
  }
}


const mapStateToProps = (state) => ({
  auth: state.auth
})

const mapDispatchToProps = {
  
}

export default connect(mapStateToProps, {AppStatusAction})(App)


