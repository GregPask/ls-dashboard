import React, { Component } from 'react'
import { Redirect } from "react-router-dom";
import axios from "axios";




export default function withAuth (ComponentToProtect){

    return class extends Component {
        constructor(){
            super();

            this.state = {
                loadging: true,
                redirect: false
            }
        }

        componentWillMount(){
            console.log("AUTH MOUNTING...");

            let token = localStorage.getItem("ls-access-token");
            console.log("token:", token);

            if(!token){
                console.log("no AXIOS REQUEST REQUIRED...LOGIN");
                this.setState({redirect: true})
            } else {
                console.log("Proceed...");

                    
                    this.setState({
                        loadging: false,
                        redirect: false
                    })
            
            }
        }

        render(){
            
            console.log(this.props);
            const { loading, redirect} = this.state;
            if(loading){
                console.log("Loading...");
            }

            if(redirect){
                return <Redirect to="/" />
            }
            
            return <ComponentToProtect {...this.props} />
        }



    }

}