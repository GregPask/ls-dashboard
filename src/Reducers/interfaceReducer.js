const initialState = {
    overlay: false,
    tab: "Business"
};



const interfaceReducer = (state = initialState, action) => {

    switch(action.type){
        
        case "TAB_PAGE":
        return {
            ...state,
            tab: action.payload
        }
    



        case "SHOW_OVERLAY":
        return {
            ...state,
            overlay: true
        }


        case "HIDE_OVERLAY":
        return {
            ...state,
            overlay: false
        }

        default: 
        return state;
    }
}


export default interfaceReducer;