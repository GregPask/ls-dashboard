
import { combineReducers } from "redux";

import userReducer from "./userReducer";
import authReducer from "./authReducer";
import grantReducer from "./grantReducer";
import interfaceReducer from "./interfaceReducer";


export default combineReducers({
    user: userReducer,
    auth: authReducer,
    grants: grantReducer,
    interface: interfaceReducer
})
