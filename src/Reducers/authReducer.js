const initialState = {
   loggedIn: false,
   loginError: ""

};



const authReducer = (state = initialState, action) => {

    switch(action.type){
        case "LOGIN":
        return {
            ...state,
            loggedIn: true
        }

        case "FAILED_LOGIN":
        return {
            ...state,
            loggedIn: false,
            loginError: "Incorrect Details, Please try again!"
        }


        case "FAILED_LOGIN_RESET":
        return {
            ...state,
            loginError: ""
        }



        case "REFRESH_USER": 
        return {
            ...state,
            loggedIn: true
        }


        default: 
        return state;
    }
}


export default authReducer;