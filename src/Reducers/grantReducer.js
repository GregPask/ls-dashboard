const initialState = {
    companiesLoading: false,
    polygon: [[58.15456,-7.64133],[51.10319,1.75159]],
    position: [53.593641, -2.2451],
    default: true,
    grantsData: [],
    grantsOrgData: [],
    loadingGrants: false,
    loadingOrgGrants: false,
    loadingRelatedGrants: false,
    location: "",
    locationGrants: [],
    relatedGrants: [],
    relatedPubs: [],
    grantOptions: {
        chart: {
            type: 'line',
            width: '100%',
            animations: {
                enabled: true,
                easing: 'ease',
                speed: 800,
                animateGradually: {
                    enabled: true,
                    delay: 150
                },
                dynamicAnimation: {
                    enabled: true,
                    speed: 550
                }
              }
          },   
          dataLabels: {
            enabled: false
          }, 
          plotOptions: {
            bar: {
              horizontal: false,
              distributed: true
            }
          },
          colors: ["#9A65B9","#4A68B4","#7468BA","#0067A9","#BC62B2","#DA60A6"],
        xaxis: {
          categories: ["ESRC","BBSRC", "MRC","EPSRC","NERC","AHRC","STFC","Innovate UK", "NC3Rs"]
        },
        yaxis: {
            labels: {
              formatter: function (value) {
                return "£" + value.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + ".00";
              }
            }
        }
      },
      
      
      series: [
        {
          name: "series-1",
          data: [0,0,0,0,0,0,0,0,0]
        }
      ]

};



const grantReducer = (state = initialState, action) => {

    switch(action.type){
        

      case "GRANTS_MAP":
      return {
        ...state,
        polygon: action.payload.polygon,
        position: action.payload.position,
        default: false,
        location: action.payload.location
      }




        case "LOADING_GRANTS":
        return {
            ...state,
            loadingGrants: true,
            grantsData: [],
            grantsOrgData: [],
            polygon: [[58.15456,-7.64133],[51.10319,1.75159]],
            position: [53.593641, -2.2451],
            locationGrants: [],
            business: [],
            relatedGrants: [],
            relatedPubs: [],
            location: action.payload
        }


        // Populate total grant money by business sectors...
        case "GRANT_DATA":
        console.log(action.payload)
        return {
            ...state,
            grantsData: action.payload.grantData,
            grantOptions: {
                ...state.grantOptions,
                xaxis: {
                    ...state.grantOptions.xaxis,
                    categories: action.payload.grantSectors
                }
            },
            series: [{
                name: "Series 1",
                data: action.payload.grantAwards
            }],
            loadingGrants: false
        }




        // Click the first grants table to groups by fundingORgNames...

        case "GRANT_TABLE_ORGS_LOADING":
        return {
          ...state,
          loadingOrgGrants: true
        }


        
        case "GRANT_TABLE_ORGS":
        return {
          ...state,
          grantsOrgData: action.payload,
          loadingOrgGrants: false
        }
        



        // We now want to view related grants & publications 


        case "LOADING_RELATED_GRANTS":
        return {
          ...state,
          loadingRelatedGrants: true
        }


        // Grants & Pubs is async so we will set loading to false when 2nd dispatch comes through...

        case "RELATED_GRANTS":
        return {
          ...state,
          relatedGrants: action.payload
        }



        case "RELATED_PUBS":
        return {
          ...state,
          relatedPubs: action.payload,
          loadingRelatedGrants: false
        }



        case "LOCATION_GRANTS":
        return {
          ...state,
          locationGrants: action.payload
        }



        default: 
        return state;
    }
}


export default grantReducer;