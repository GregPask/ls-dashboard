const initialState = {
    user: "greg",
    polygon: [[58.15456,-7.64133],[51.10319,1.75159]],
    position: [53.593641, -2.2451],
    default: true,
    polygon2: [[58.15456,-7.64133],[51.10319,1.75159]],
    position2: [53.593641, -2.2451],
    default2: true,
    mapCoords: [],
    loadingBusiness: false,
    businessChart: [
        {
          name: "series-1",
          data: [0,0,0,0,0,0,0,0]
        }
      ],
    businessTable: [],
    business: [],
    location: "",
    businessOptions: {
        grid: {
            show: true,         
          },
        chart: {           
             animations: {
                enabled: true,
                easing: 'ease-in',
                speed: 800,
              
                dynamicAnimation: {
                    enabled: true,
                    speed: 850
                }
              },
            type: 'line',
            width: '100%',
          },
          plotOptions: {
          bar: {
            horizontal: true,
            distributed: true
          }
        },
        colors: ["#9A65B9","#4A68B4","#7468BA","#0067A9","#BC62B2"],
        dataLabels: {
          enabled: false
        },
        xaxis: {
            title: {
                text: "Number of Businesses"
            },
            categories: ["Animal Health", "Diagnostics","Generics Distribution And Pharmacies","Manufature", "Medtech",
            'Research and Development', "Service","Specialty Pharma"]
        
        }
      }
};



const userReducer = (state = initialState, action) => {

    switch(action.type){
        
        case "LOADING_BUSINESSES":
        return {
            ...state,
            companiesLoading: true,
            business: [],
            mapCoords: [],
            loadingBusiness: true
        }

        case "DASHBOARD_MAP":
        return {
            ...state,
            polygon: action.payload.polygon,
            position: action.payload.position,
            default: false,
            location: action.payload.location
        }


        case "DASHBOARD_MAP_RESEARCH":
        return {
            ...state,
            polygon2: action.payload.polygon,
            position2: action.payload.position,
            default2: false
        }



        case "BUSINESS_CHART":
        console.log(action.payload);
        return {
            ...state, 
            businessChart: [{
                name: "Number of Businesses",
                data: action.payload.series
            },
        ],
            businessOptions: {
                ...state.businessOptions,

                    xaxis: {
                    ...state.businessOptions.xaxis,
                    categories: action.payload.categories
                }
            }
        }


        // We hit search and this will populate the 1st businesses table.
        case "BUSINESS_TABLE":
        return {
            ...state,
            businessTable: action.payload,
            companiesLoading: false
        }


           // We hit search and this will populate the 1st businesses table.
           case "CLICKED_BUSINESS_TABLE":
           console.log(action.payload);


           let index = state.businessTable.findIndex((row,index) => row.Name === action.payload.Name);
           console.log(index);


           let newRow = {
               ...action.payload,
               selected: true
           }

           console.log(newRow);
           let v3 = state.businessTable.splice(index,1,newRow);
           console.log(v3);


           return {
               ...state,
               businessTable: state.businessTable
           }
   


        // Click a row in business table to view specific details
        case "BUSINESS_TABLE_VIEW":
        return {
            ...state,
            business: action.payload,

            
        }

        case "DISABLE_BTN": 
        return {
            ...state,
            loadingBusiness: false
        }


        case "LOADING_BUSINESS":
        return {
            ...state,
            loadingBusiness: true
        }



        case "SET_MAP_MARKERS":
        return {
            ...state,
            mapCoords: [action.payload[0], action.payload[1]]
        }


        default: 
        return state;
    }
}


export default userReducer;