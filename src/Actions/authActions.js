import axios from "axios";




// When a user submits a form.
export const loginAction = (user,props) => dispatch => {

    console.log("login Action...");

    axios.post("https://insights.impactdatametrics.com:17475/api/login",user)
    .then((res) => {
        console.log(res.data);
            if(res.data.access_token){
            console.log("WE HAVE A TOKEN");

            localStorage.setItem("ls-access-token",res.data.access_token);
            localStorage.setItem("ls-overlay-token", true);
            props.history.push("/dashboard");

                dispatch({
                    type: "LOGIN"
                })



                } else {
                    dispatch({
                        type: "FAILED_LOGIN"
                    })


                    setTimeout(() => {
                       dispatch({
                        type: "FAILED_LOGIN_RESET"
                       })
                    }, 2000);
                }
    })
    .catch((err) => console.log(err));   
};





export const AppStatusAction = (config) => dispatch => {

    console.log(config);

        if(config.accessToken){

            console.log("WE HAVE A USER Keep them logged in");

            dispatch({
                type: "REFRESH_USER"
            })


        } else {
            console.log("We have no token stay logged out");
            // alert("No token");
            
        }


}

