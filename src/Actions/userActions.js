import axios from "axios";
import _ from "lodash";
import Axios from "axios";


export const searchAction = (config) => async dispatch => {


    let token = localStorage.getItem("ls-access-token");
    if (!token) {
        return;
    }

    let data = {
        code: config.code
    }


    dispatch({
        type: "LOADING_BUSINESSES"
    })


    let businessBody =  {
        "query": "SELECT o.company_name as \"Name\",o.description as \"About\",o.category as \"Category\",z.latitude,z.longitude,right(concat('00000', o.new_cn),8) as \"Company Number\" FROM postcodelatlng z,gss.shapes s,\"#Core-LifeSciences\".lifesciences o WHERE st_contains(geom,pos) AND text(z.postcode) = text(o.postcode) AND s.gss = '" + config.code + "' AND category = ANY('{Research and Development,Service,Manufacture,Medtech,Generics Distribution and Pharmacies,Specialty Pharma,Animal Health,Diagnostics,Major Pharma}')"
   }
   


   axios.post("https://insights.impactdatametrics.com/api/sql/dict/", businessBody,{
       headers: {
           "Content-Type": "application/json"
       }
   })
   .then((res) => {
       console.log(res.data);

       let data = res.data;
       dispatch({
           type: "BUSINESS_TABLE",
           payload: data
       });

       setTimeout(() => {
           dispatch({
               type: "DISABLE_BTN"
           })
       }, 0);
   })



    //GET POLYGON SHAPES FROM API 
    axios.get(`https://insights.impactdatametrics.com:17475/api/gss/geometry/${config.code}/?format=geojson_only`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then((res) => {
            console.log(res.data);
            //Api returns lat/lng cooconrdinates different format to leaflet. we need to reverse them.
            let polygon = res.data.geometry.coordinates[0][0];
            polygon.reverse();
            let reversedCoordinates = polygon.map((poly, index) => poly.reverse());

            dispatch({
                type: "DASHBOARD_MAP",
                payload: {
                    polygon: reversedCoordinates,
                    position: [reversedCoordinates[0][0], reversedCoordinates[0][1]],
                    location: res.data.properties.description
                }
            })

            // dispatch({
            //     type: "LOADING_GRANTS",
            //     payload: res.data.properties.description
            // })
        });



        let numbersBody = {
            "query": "SELECT category, count(category) FROM postcodelatlng z,gss.shapes s,\"#Core-LifeSciences\".lifesciences o WHERE st_contains(geom,pos) AND text(z.postcode) = text(o.postcode) AND s.gss = '" + config.code + "' AND category = ANY('{Research and Development,Service,Manufacture,Medtech,Generics Distribution and Pharmacies,Specialty Pharma,Animal Health,Diagnostics,Major Pharma}') Group by (category)"
           }
           
           
           
           
           
           


    axios.post("https://insights.impactdatametrics.com/api/sql/dict/", numbersBody,{
        headers: {
            "Content-Type": "application/json"
        }
        })
        .then((res) => {

            console.log(res.data);

        
                let data = res.data;
                console.log(data);

                let categories = [];

                let series = [];

                data.forEach(element => {
                    categories.push(element.category);
                    series.push(parseInt(element.count));
                    series.push()
                });


            console.log(categories);
            console.log(series);


           

       

            dispatch({
                type: "BUSINESS_CHART",
                payload: {
                    series,
                    categories,
                }
            })
        })




      


        // axios.get(`https://ls-api-idm.herokuapp.com/grants/${config.code}`)
        // .then((res) => {
        //     console.log(res.data);
        //     let grant = res.data.result.rows;
        //     let grantSectors = {
        //         ESRC: 0,
        //         BBSRC: 0,
        //         MRC: 0,
        //         EPSRC: 0,
        //         NERC: 0,
        //         AHRC: 0,
        //         STFC: 0,
        //         "Innovate UK": 0,
        //         NC3Rs: 0
        //     };

        //     for (let x in grant) {
        //         switch (grant[x]["Funder"]) {

        //             case "ESRC":
        //                 grantSectors["ESRC"] += grant[x].Value;
        //                 break;

        //             case "BBSRC":
        //                 grantSectors["BBSRC"] += grant[x].Value;
        //                 break;

        //             case "MRC":
        //                 grant[x].Value !== "NULL" ? grantSectors["MRC"] += grant[x].Value : grant[x].Value += 0;
        //                 break;

        //             case "EPSRC":
        //                 grantSectors["EPSRC"] += grant[x].Value;
        //                 break;

        //             case "NERC":
        //                 grant[x].Value !== "NULL" ? grantSectors["NERC"] += grant[x].Value : grant[x].Value += 0;
        //                 break;

        //             case "AHRC":
        //                 grantSectors["AHRC"] += grant[x].Value;
        //                 break;

        //             case "STFC":
        //                 grant[x].Value !== "NULL" ? grantSectors["STFC"] += grant[x].Value : grant[x].Value += 0;
        //                 break;

        //             case "Innovate UK":
        //                 grantSectors["Innovate UK"] += grant[x].Value;
        //                 break;


        //             case "NC3Rs":
        //                 grant[x].Value !== "NULL" ? grantSectors["NC3Rs"] += grant[x].Value : grant[x].Value += 0;
        //                 break;


        //             default:
        //                 console.log("Hello");
        //         }

        //     }

        //     console.log(grantSectors);

        //     // If A sector has zero pounds from grants we can delete it from the chart data...
        //     Object.entries(grantSectors).forEach(element => {
        //         console.log(element);
        //         if (element[1] === 0) {
        //             //   delete g
        //             delete grantSectors[element[0]];

        //         }
        //     });


        //     dispatch({
        //         type: "GRANT_DATA",
        //         payload: {
        //             grantData: grant,
        //             grantSectors: [...Object.keys(grantSectors)],
        //             grantAwards: [...Object.values(grantSectors)]
        //         }
        //     })




        // })
        // .catch((err) => console.log(err));

}



export const grantsSearchAction = (grantsData) => async dispatch => {

    console.log("Searching for grants", grantsData);


    let token = localStorage.getItem("ls-access-token");
    if (!token) {
        return;
    } else {
        console.log("No token....");
    }


    //Populate shape goems

    //GET POLYGON SHAPES FROM API 
    await axios.get(`https://insights.impactdatametrics.com:17475/api/gss/geometry/${grantsData}/?format=geojson_only`, {
            headers: {
                "Authorization": `Bearer ${token}`
            }
        })
        .then((res) => {

            console.log(res.data);



            //Api returns lat/lng coordinates different format to leaflet. we need to reverse them.
            let polygon = res.data.geometry.coordinates[0][0];
            polygon.reverse();
            let reversedCoordinates = polygon.map((poly, index) => poly.reverse());

            dispatch({
                type: "LOADING_GRANTS",
                payload: res.data.properties.description
            })


            dispatch({
                type: "GRANTS_MAP",
                payload: {
                    polygon: reversedCoordinates,
                    position: [reversedCoordinates[0][0], reversedCoordinates[0][1]],
                    location: res.data.properties.description
                }
            })

        });




        let body = 
        {
            "query": "SELECT g.\"FundingOrgName\" as \"Funder\", g.\"LeadROName\" as \"Award Recipient\", sum(g.\"AwardPounds\") as \"Value\" FROM geoff.rcuk_grants g WHERE \"LeadROId\" = ANY (SELECT o.\"OrganisationId\" FROM    postcodelatlng z,gss.shapes s, geoff.rcuk_organisations o WHERE st_contains(geom,pos) AND text(z.postcode) = o.\"PostCode\" AND s.gss = ANY('{" + grantsData + "}')) AND g.\"Status\" = 'Active' AND g.\"AwardPounds\" > 0 GROUP BY (g.\"FundingOrgName\", g.\"LeadROName\")"
         }
       




    //  Get grants based on gss code....

   await axios.post(`https://insights.impactdatametrics.com/api/sql/dict/`, body,  {
       header: {
           "Content-Type": "application/json"
       }
   })
        .then((res) => {
            console.log(res.data);
            let grant = res.data;
            let grantSectors = {
                ESRC: 0,
                BBSRC: 0,
                MRC: 0,
                EPSRC: 0,
                NERC: 0,
                AHRC: 0,
                STFC: 0,
                "Innovate UK": 0,
                NC3Rs: 0
            };

            for (let x in grant) {
                switch (grant[x]["Funder"]) {

                    case "ESRC":
                        grantSectors["ESRC"] += grant[x].Value;
                        break;

                    case "BBSRC":
                        grantSectors["BBSRC"] += grant[x].Value;
                        break;

                    case "MRC":
                        grant[x].Value !== "NULL" ? grantSectors["MRC"] += grant[x].Value : grant[x].Value += 0;
                        break;

                    case "EPSRC":
                        grantSectors["EPSRC"] += grant[x].Value;
                        break;

                    case "NERC":
                        grant[x].Value !== "NULL" ? grantSectors["NERC"] += grant[x].Value : grant[x].Value += 0;
                        break;

                    case "AHRC":
                        grantSectors["AHRC"] += grant[x].Value;
                        break;

                    case "STFC":
                        grant[x].Value !== "NULL" ? grantSectors["STFC"] += grant[x].Value : grant[x].Value += 0;
                        break;

                    case "Innovate UK":
                        grantSectors["Innovate UK"] += grant[x].Value;
                        break;


                    case "NC3Rs":
                        grant[x].Value !== "NULL" ? grantSectors["NC3Rs"] += grant[x].Value : grant[x].Value += 0;
                        break;


                    default:
                        console.log("Hello");
                }

            }

            console.log(grantSectors);

            // If A sector has zero pounds from grants we can delete it from the chart data...
            Object.entries(grantSectors).forEach(element => {
                console.log(element);
                if (element[1] === 0) {
                    //   delete g
                    delete grantSectors[element[0]];

                }
            });


            dispatch({
                type: "GRANT_DATA",
                payload: {
                    grantData: grant,
                    grantSectors: [...Object.keys(grantSectors)],
                    grantAwards: [...Object.values(grantSectors)]
                }
            })




        })
        .catch((err) => console.log(err));


        let locationGrants = {
            "query": "SELECT distinct(z.postcode, z.longitude, z.latitude, o.\"OrganisationName\"), z.postcode, z.longitude, z.latitude, o.\"OrganisationName\" FROM postcodelatlng z,gss.shapes s, geoff.rcuk_organisations o JOIN geoff.rcuk_grants g ON o.\"OrganisationId\" = g.\"LeadROId\" WHERE st_contains(geom,pos) AND text(z.postcode) = o.\"PostCode\" AND s.gss = %(lead)s  AND g.\"Status\" = 'Active' AND g.\"AwardPounds\" > 0",
            "params":{"lead": grantsData } 
        }

        // E12000004

   await axios.post(`https://insights.impactdatametrics.com/api/sql/dict/`, locationGrants,  {
    header: {
        "Content-Type": "application/json"
    }
})
     .then((res) => {
        console.log(res.data)

         dispatch({
           type: "LOCATION_GRANTS",
           payload: res.data
         })




     })
     .catch((err) => console.log(err));

}



// Click Businesses Table number 1

export const businessTableAction = (rowInfo) => dispatch => {

    console.log(rowInfo, "aCTION");
    // console.log("Business Table action", rowInfo);


    document.getElementById("dashboard-right-info").scrollTop = 0;

    dispatch({
        type: "LOADING_BUSINESS",
    
    });


    let number = rowInfo["Company Number"];

    let body10 = { "query": "Select c.\"CompanyName\" as \"Organisation\", l.description as \"Description\", l.postcode as \"Operating Postcode\", c.\"RegAddress\" ->> 'PostCode' as \"Registered Address Postcode\", c.\"RegAddress\" ->> 'AddressLine1' as \"Address Line 1\",   c.\"RegAddress\" ->> 'AddressLine2' as \"Address Line 2\", l.category as \"Category\", c.\"CompanyCategory\" as \"Company Type\", c.\"CompanyStatus\" as \"Status\", c.\"CountryOfOrigin\" as \"Home Country\",    c.\"IncorporationDate\" as \"Date Incorporated\", c.\"SICCode\" as \"SIC Code\", c.\"Accounts\" ->> 'NextDueDate' as \"Next Accounts Due\", c.\"Accounts\" ->> 'LastMadeUpDate' as \"Last Accounts\", c.\"Accounts\" ->> 'AccountCategory' as \"Accounts Type\", x.rcd -> 'CRN' ->> 'value' as \"Company Number\", x.rcd -> 'Equity' ->> 'value' as \"Equity Value\", x.rcd -> 'Equity' ->> 'unitRef' as \"Equity Value Unit\", x.rcd -> 'Debtors' ->> 'value' as \"Debtors Value\", x.rcd -> 'Debtors' ->> 'unitRef' as \"Debtors Value Unit\", x.rcd -> 'OtherDebtors' ->> 'value' as \"Other Debtors Value\", x.rcd -> 'OtherDebtors' ->> 'unitRef' as \"Other Debtors Value Unit\",    x.rcd -> 'Creditors' ->> 'value' as \"Creditors Value\", x.rcd -> 'Creditors' ->> 'unitRef' as \"Creditors Value Unit\", x.rcd -> 'CurrentAssets' ->> 'value' as \"Current Assets Value\", x.rcd -> 'CurrentAssets' ->> 'unitRef' as \"Current Assets Value Unit\", x.rcd -> 'CashBankOnHand' ->> 'value' as \"Cash at Bank and on Hand Assets Value\", x.rcd -> 'CashBankOnHand' ->> 'unitRef' as \"Cash at Bank and on Hand Assets Value Unit\", x.rcd -> 'DeferredIncome' ->> 'value' as \"Deferred Income Value\", x.rcd -> 'DeferredIncome' ->> 'unitRef' as \"Deferred Income Value Unit\", x.rcd -> 'TotalAssetsLessCurrentLiabilities' ->> 'value' as \"Net Assets/Liabilities Value\", x.rcd -> 'TotalAssetsLessCurrentLiabilities' ->> 'unitRef' as \"Net Assets/Liabilities Value Unit\" From \"#Core-LifeSciences\".lifesciences l JOIN  \"@COMPANIESHOUSE\".companies c on right(concat('00000', l.new_cn),8)=c.\"CompanyNumber\" left JOIN  \"@XBRL\".company x on right(concat('00000', l.new_cn),8)=x.crn Where right(concat('00000', l.new_cn),8) =  %(lead)s",
    "params":{"lead": rowInfo['Company Number'] } 
     }
    



            axios.post("https://insights.impactdatametrics.com/api/sql/dict/", body10,{
                headers: {
                    "Content-Type": "application/json"
                }
            })
            .then((res) => {
             
        dispatch({
            type: "BUSINESS_TABLE_VIEW",
            payload: res.data
        });

        dispatch({
            type: "SET_MAP_MARKERS",
            payload: [rowInfo.latitude,rowInfo.longitude]
        });



        dispatch({
            type: "CLICKED_BUSINESS_TABLE",
            payload: rowInfo
        })


        setTimeout(() => {
            dispatch({
                type: "DISABLE_BTN"
            })
        }, 1000);


        })
        


        // })
        .catch((err) => console.log(err));
};



// Click first grants table to match my funders
export const grantsTableAction = (funder) => dispatch => {
    console.log("clICK TABLE ACTION,....",funder);
    
    dispatch({type: "GRANT_TABLE_ORGS_LOADING"})



let body2 =  {
    "query": "Select \"Department\", concat(\"PIFirstName\", ' ', \"PISurname\") as \"Lead Investigator\",sum(\"AwardPounds\") as \"Value\",\"FundingOrgName\" as \"Funder\",\"LeadROName\" as \"Award Recipient\", \"ProjectReference\" as \"Reference Number\" FROM geoff.rcuk_grants WHERE \"LeadROName\" = %(lead)s AND (\"AwardPounds\") > 0 AND \"FundingOrgName\" = %(lead2)s GROUP BY (\"FundingOrgName\",\"LeadROName\",\"Department\", concat(\"PIFirstName\", ' ', \"PISurname\"), \"ProjectReference\") Order by (\"Department\") Asc",
    
    "params":{"lead": funder.LeadROName, "lead2": funder.FundingOrgName } 
 }




    axios.post(`https://insights.impactdatametrics.com/api/sql/dict/`, body2,  {
    headers: {
        "Content-Type": "application/json"
    }
    })
     .then((res) => {
        console.log(res.data);

           dispatch({
        type: "GRANT_TABLE_ORGS",
        payload: res.data
    })
     })
     .catch((err) => console.log(err));


      
    // axios.post("https://ls-api-idm.herokuapp.com/grants1", funder)
    // .then((res) => {
    //   console.log(res.data);


    //   dispatch({
    //     type: "GRANT_TABLE_ORGS",
    //     payload: res.data.result.rows
    // })

    // })
    // .catch((err) => console.log(err));

}




// TODO *** HAVE A LOOK AT related grants numbers currently limited to 600..........

// Click 2nd grants table to get related publications and grants....
export const grantsTable2Action = (info) => async dispatch => {


    console.log("2nd grants table", info);

    dispatch({
        type: "LOADING_RELATED_GRANTS"

    })




    let bodygrants =   {
        "query": "SELECT concat(\"PIFirstName\", ' ', \"PISurname\") as \"Lead Investigator\",\"LeadROName\" as \"Award Recipient\",\"Department\",\"ProjectCategory\" as \"Project Type\",\"Title\",\"AwardPounds\" as \"Value\",TO_DATE(\"EndDate\", 'DD/MM/YYYY') as \"End Date\" FROM geoff.rcuk_grants WHERE \"PIId\" = ANY (SELECT \"PIId\" FROM geoff.rcuk_grants WHERE \"ProjectReference\" =   %(lead)s)",
          
        "params":{"lead": info.id }
        }
          
    
    
    
    
        axios.post(`https://insights.impactdatametrics.com/api/sql/dict/`, bodygrants,  {
        headers: {
            "Content-Type": "application/json"
        }
        })
         .then((res) => {
            console.log(res.data);

                dispatch({
                    type: "RELATED_GRANTS",
                    payload: res.data.slice(0,600)
                })
    
            //    dispatch({
            // type: "GRANT_TABLE_ORGS",
            // payload: res.data
        })
         
         .catch((err) => console.log(err));






            let bodyPublications =  {
                "query": "SELECT \"PublicationType\" as \"Article Type\",\"Author\" as \"Lead Author\",\"Year\",\"Title\",\"JournalName\" as \"Publication Name\",\"Volume\",\"Issue\",\"Pages\"FROM geoff.rcuk_publications WHERE \"ProjectReference\" = ANY (SELECT \"ProjectReference\" FROM geoff.rcuk_grants WHERE \"PIId\" = ANY (SELECT \"PIId\" FROM geoff.rcuk_grants WHERE \"ProjectReference\" = %(lead)s))",
                
                 "params":{"lead": info.id } 
                  }
                 





         axios.post(`https://insights.impactdatametrics.com/api/sql/dict/`, bodyPublications,  {
            headers: {
                "Content-Type": "application/json"
            }
            })
             .then((res) => {
                console.log(res.data);
    
                    dispatch({
                    type: "RELATED_PUBS",
                    payload: res.data.slice(0,600)
                })


            })
             
             .catch((err) => console.log(err));
    







    // await axios.post(`https://ls-api-idm.herokuapp.com/relatedgrants`, info)
    // .then((res) => {
    //   console.log(res.data);


    //   dispatch({
    //     type: "RELATED_GRANTS",
    //     payload: res.data.result.rows.slice(0,600)
    // })

    // })
    // .catch((err) => console.log(err));



    // await axios.post(`https://ls-api-idm.herokuapp.com/relatedpubs`,info)
    // .then((res) => {
    //   console.log(res.data);


    //   dispatch({
    //     type: "RELATED_PUBS",
    //     payload: res.data.result.rows.slice(0,600)
    // })

    // })
    // .catch((err) => console.log(err));


    // let grants = axios.post(`https://ls-api-idm.herokuapp.com/relatedgrants`,info);
    // let pubs = axios.post(`https://ls-api-idm.herokuapp.com/relatedpubs`,info);


    // Promise.all([grants,pubs])
    // .then((res) => {
    //     console.log(res.data);
    // })
    // .catch((err) => console.log(err));




}






// NEw grants queries -------------------------------------


//  --------- Query for Grants summary table (top right)




// SELECT g."FundingOrgName" as "Funder",
// g."LeadROName" as "Award Recipient",
// sum(g."AwardPounds") as "Value" 
// FROM postcodelatlng z,gss.shapes s, geoff.rcuk_organisations o 
// JOIN geoff.rcuk_grants g
// ON o."OrganisationId" = g."LeadROId"
//  	WHERE st_contains(geom,pos) 
//  	AND text(z.postcode) = o."PostCode" 
//  	AND s.gss = ANY('{E37000001}')
// 	AND g."Status" = 'Active'
// 	AND g."AwardPounds" > 0
// 	GROUP BY (g."FundingOrgName", g."LeadROName")




//  -----------------------------------------------------



//  To get lat long of unique locations of organisations in a gss area
// SELECT distinct(z.postcode, z.longitude, z.latitude, o."OrganisationName")
// FROM postcodelatlng z,gss.shapes s, geoff.rcuk_organisations o 
// JOIN geoff.rcuk_grants g
// ON o."OrganisationId" = g."LeadROId"
//  	WHERE st_contains(geom,pos) 
//  	AND text(z.postcode) = o."PostCode" 
//  	AND s.gss = ANY('{E37000001}')
// 	AND g."Status" = 'Active'
// 	AND g."AwardPounds" > 0